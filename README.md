# connatix-player-sdk-objc

Special repository for Swift Package Manager integration.

iOS native SDK for the Connatix player. Compatible with Objective-C.

We recommend using `connatix-player-sdk` if you have a Swift only integration.

Please consult the official Connatix support documentation for further information.